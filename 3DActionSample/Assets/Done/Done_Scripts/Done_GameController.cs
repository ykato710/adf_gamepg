﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Done_GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public GameObject player;
    public GameObject boss;
    public GameObject[] items;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnRate;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public float bossWaveWait;
    private bool isBossKilled;

    //for stocks
    public GameObject StockIcons;
    public float respornWait;
    public int stocks;
    private int checkpoint;

    //for shield
    private bool isShield;
    private float shieldLimit;
    public float shieldWait;

    //for invincible effect
    private bool isInvincible;
    private float iLimit;
    private bool isVisible;

    //for special bullets
    public int maxBulletsNum;

    //for missile
    private int missiles;
    public GameObject missileIcons;

    //for blast bullets
    private int blasts;
    public GameObject blastIcons;

    //for bomb
    public GameObject explosion;
    public GameObject bigExplosion;
    private bool isBombEffective;
    public float bombWait;
    private int bombs;
    public GameObject bombIcons;

    public GUIText scoreText;
    public GUIText highScoreText;
    public GUIText restartText;
    public GUIText gameOverText;

    private bool gameOver;
    private bool restart;
    private int score;
    private int highScore;

    void Start()
    {
        checkpoint = 0;
        missiles = 0;
        bombs = 2;
        isBossKilled = true;
        isInvincible = true;
        isVisible = true;
        BombReset();
        KillSpark();
        iLimit = Time.time;
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        highScore = 0;
        UpdateScore();
        UpdateStocks();
        UpdateBulletsIcons();
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        if (shieldLimit < Time.time && isShield)
        {
            KillSpark();
        }

        if (iLimit < Time.time && isInvincible)
        {
            UpdatePlayerRenderer(true);
            UpdatePlayerState();
            isBombEffective = true;
        }
        else if (isInvincible && isBombEffective)
        {
            UpdatePlayerRenderer(!isVisible);
        }
        else if (isInvincible)
        {
            UpdatePlayerRenderer(true);
        }
    }

    private void UpdatePlayerRenderer(bool next)
    {
        GameObject newPlayer = GameObject.Find("Player(Clone)");
        if (newPlayer == null)
        {
            newPlayer = GameObject.Find("Player");
        }

        GameObject mesh;
        mesh = newPlayer.transform.Find("Mesh_SD_unitychan").gameObject;
        GameObject body = mesh.transform.Find("_body").gameObject;
        GameObject eye = mesh.transform.Find("_eye").gameObject;
        GameObject Fhair = mesh.transform.Find("_Fhair").gameObject;
        GameObject Fhair2 = mesh.transform.Find("_Fhair2").gameObject;
        GameObject head = mesh.transform.Find("_head").gameObject;
        GameObject face = newPlayer.transform.Find("Character1_Reference/center/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_Neck/Character1_Head/_face").gameObject;

        body.renderer.enabled = next;
        eye.renderer.enabled = next;
        Fhair.renderer.enabled = next;
        Fhair2.renderer.enabled = next;
        head.renderer.enabled = next;
        face.renderer.enabled = next;
        isVisible = next;
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {

            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(bossWaveWait);
            if (isBossKilled)
            {
                GameObject bigHazard = boss;
                Quaternion spawnRotationBoss = Quaternion.identity;
                Instantiate(bigHazard, new Vector3(0, 0, 15), spawnRotationBoss);
                UpdateBossStatus(false);
                yield return new WaitForSeconds(waveWait);
            }

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        if (score / 500 == checkpoint && (score + newScoreValue) / 500 != checkpoint)
        {
            checkpoint++;
            stocks++;
        }

        score += newScoreValue;
        if (score > highScore)
            highScore = score;

        UpdateScore();
        UpdateStocks();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
        highScoreText.text = "Score: " + highScore;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

    public void PlayerKilled()
    {
        if (stocks <= 0)
        {
            GameOver();
        }
        else
        {
            stocks--;
            bombs = 2;
            Quaternion spawnRotation = Quaternion.Euler(40, 0, 0);
            GameObject newPlayer = Instantiate(player, new Vector3(0.0f, -0.5f, 0.0f), spawnRotation) as GameObject;
            newPlayer.tag = "Invincible";
            BombReset();
            KillSpark();
            UpdateBulletsIcons();
            InvincibleEffect();
            iLimit = Time.time + respornWait;
            Invoke("Check", 5);
        }
    }

    public void UpdatePlayerState()
    {
        GameObject newPlayer = GameObject.Find("Player(Clone)");
        if (newPlayer == null)
        {
            newPlayer = GameObject.Find("Player");
        }
        newPlayer.tag = "Player";
        isInvincible = false;
    }

    public void KillSpark()
    {
        GameObject newPlayer = SetEnable("Spark", false);
        isShield = false;
        Done_PlayerController pcon = newPlayer.GetComponent<Done_PlayerController>();
        pcon.SetShotEnable(true);
    }

    public void Shield()
    {
        GameObject newPlayer = GameObject.Find("Player(Clone)");
        if (newPlayer == null)
        {
            newPlayer = GameObject.Find("Player");
        }

        Done_PlayerController pcon = newPlayer.GetComponent<Done_PlayerController>();
        pcon.SetShotEnable(false);

        if (newPlayer.transform.Find("Spark").gameObject.activeSelf == true)
            newPlayer.transform.Find("Spark").gameObject.SetActive(false);
        newPlayer.transform.Find("Spark").gameObject.SetActive(true);
        isShield = true;
        shieldLimit = Time.time + shieldWait;
    }

    public void SpawnItem(Vector3 position, Quaternion rotation)
    {
        if (Random.value <= spawnRate)
        {
            GameObject item = items[Random.Range(0, items.Length)];
            Instantiate(item, position, rotation);
        }
    }

    private void UpdateStocks()
    {

        GameObject stock1 = StockIcons.transform.FindChild("Stock1").gameObject;
        GameObject stock2 = StockIcons.transform.FindChild("Stock2").gameObject;
        GameObject stock3 = StockIcons.transform.FindChild("Stock3").gameObject;
        GameObject stock4 = StockIcons.transform.FindChild("Stock4").gameObject;
        GameObject stock5 = StockIcons.transform.FindChild("Stock5").gameObject;

        stock1.SetActive(false);
        stock2.SetActive(false);
        stock3.SetActive(false);
        stock4.SetActive(false);
        stock5.SetActive(false);

        if (stocks >= 1)
        {
            stock1.SetActive(true);
        }
        if (stocks >= 2)
        {
            stock2.SetActive(true);
        }
        if (stocks >= 3)
        {
            stock3.SetActive(true);
        }
        if (stocks >= 4)
        {
            stock4.SetActive(true);
        }
        if (stocks >= 5)
        {
            stock5.SetActive(true);
        }
    }

    public void UpdateBossStatus(bool value)
    {
        isBossKilled = value;
    }

    private void InvincibleEffect()
    {
        isInvincible = true;
    }

    private void UpdateBulletsIcons()
    {
        GameObject missile1 = missileIcons.transform.FindChild("Missile1").gameObject;
        GameObject missile2 = missileIcons.transform.FindChild("Missile2").gameObject;
        GameObject missile3 = missileIcons.transform.FindChild("Missile3").gameObject;
        GameObject missile4 = missileIcons.transform.FindChild("Missile4").gameObject;
        GameObject missile5 = missileIcons.transform.FindChild("Missile5").gameObject;

        missile1.SetActive(false);
        missile2.SetActive(false);
        missile3.SetActive(false);
        missile4.SetActive(false);
        missile5.SetActive(false);

        if (missiles >= 1)
        {
            missile1.SetActive(true);
        }
        if (missiles >= 2)
        {
            missile2.SetActive(true);
        }
        if (missiles >= 3)
        {
            missile3.SetActive(true);
        }
        if (missiles >= 4)
        {
            missile4.SetActive(true);
        }
        if (missiles >= 5)
        {
            missile5.SetActive(true);
        }

        GameObject blast1 = blastIcons.transform.FindChild("Blast1").gameObject;
        GameObject blast2 = blastIcons.transform.FindChild("Blast2").gameObject;
        GameObject blast3 = blastIcons.transform.FindChild("Blast3").gameObject;
        GameObject blast4 = blastIcons.transform.FindChild("Blast4").gameObject;
        GameObject blast5 = blastIcons.transform.FindChild("Blast5").gameObject;

        blast1.SetActive(false);
        blast2.SetActive(false);
        blast3.SetActive(false);
        blast4.SetActive(false);
        blast5.SetActive(false);

        if (blasts >= 1)
        {
            blast1.SetActive(true);
        }
        if (blasts >= 2)
        {
            blast2.SetActive(true);
        }
        if (blasts >= 3)
        {
            blast3.SetActive(true);
        }
        if (blasts >= 4)
        {
            blast4.SetActive(true);
        }
        if (blasts >= 5)
        {
            blast5.SetActive(true);
        }

        GameObject bomb1 = bombIcons.transform.FindChild("Bomb1").gameObject;
        GameObject bomb2 = bombIcons.transform.FindChild("Bomb2").gameObject;
        GameObject bomb3 = bombIcons.transform.FindChild("Bomb3").gameObject;
        GameObject bomb4 = bombIcons.transform.FindChild("Bomb4").gameObject;
        GameObject bomb5 = bombIcons.transform.FindChild("Bomb5").gameObject;

        bomb1.SetActive(false);
        bomb2.SetActive(false);
        bomb3.SetActive(false);
        bomb4.SetActive(false);
        bomb5.SetActive(false);

        if (bombs >= 1)
        {
            bomb1.SetActive(true);
        }
        if (bombs >= 2)
        {
            bomb2.SetActive(true);
        }
        if (bombs >= 3)
        {
            bomb3.SetActive(true);
        }
        if (bombs >= 4)
        {
            bomb4.SetActive(true);
        }
        if (bombs >= 5)
        {
            bomb5.SetActive(true);
        }
    }

    public void AddMissiles()
    {
        if (missiles < maxBulletsNum)
            missiles++;
        UpdateBulletsIcons();
    }

    public void AddBlasts()
    {
        if (blasts < maxBulletsNum)
            blasts++;
        UpdateBulletsIcons();
    }

    public void AddBombs()
    {
        if (bombs < maxBulletsNum)
            bombs++;
        UpdateBulletsIcons();
    }

    public int GetRestMissiles()
    {
        return missiles;
    }

    public int GetRestBlasts()
    {
        return blasts;
    }

    public int GetRestBombs()
    {
        return bombs;
    }

    public void ReduceRestMissiles()
    {
        if (missiles > 0)
            missiles--;
        UpdateBulletsIcons();
    }

    public void ReduceRestBlasts()
    {
        if (blasts > 0)
            blasts--;
        UpdateBulletsIcons();
    }

    public void ReduceRestBombs()
    {
        if (bombs > 0)
            bombs--;
        UpdateBulletsIcons();
    }

    public GameObject SetEnable(string str, bool value)
    {
        GameObject newPlayer = GameObject.FindWithTag("Player");
        if (newPlayer == null)
        {
            newPlayer = GameObject.FindWithTag("Invincible");
        }
        newPlayer.transform.Find(str).gameObject.SetActive(value);
        return newPlayer;
    }

    public void Bomb()
    {
        if (isBombEffective)
        {
            bombs--;
            UpdateBulletsIcons();
            isBombEffective = false;
            isInvincible = true;
            SetEnable("Bomb", true);
            iLimit = Time.time + bombWait;
            GameObject newPlayer = GameObject.Find("Player(Clone)");
            if (newPlayer == null)
            {
                newPlayer = GameObject.Find("Player");
            }
            newPlayer.tag = "Invincible";
            Invoke("KillThemAll", 3.5f);
        }
    }

    public void KillThemAll()
    {
        foreach (GameObject obs in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (obs.name.Contains("Boss"))
            {
                Instantiate(bigExplosion, obs.transform.position, obs.transform.rotation);
                UpdateBossStatus(true);
                SpawnItem(obs.transform.position, obs.transform.rotation);
            }
            else
            {
                Instantiate(explosion, obs.transform.position, obs.transform.rotation);
                SpawnItem(obs.transform.position, obs.transform.rotation);
            }
            Destroy(obs);
        }
        Invoke("BombReset", 0.5f);
    }

    public void BombReset()
    {
        SetEnable("Bomb", false);
        isBombEffective = true;
    }
}