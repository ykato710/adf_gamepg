﻿using UnityEngine;
using System.Collections;

public class ItemScript : MonoBehaviour
{

    private Done_GameController gameController;
    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player" || other.tag == "Shield" || other.tag == "Invincible")
        {
            GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
            if (gameControllerObject != null)
            {
                gameController = gameControllerObject.GetComponent<Done_GameController>();
            }
            if (gameController == null)
            {
                Debug.Log("Cannot find 'GameController' script");
            }
            if (name.Contains("shield"))
                gameController.Shield();
            else if (name.Contains("missile"))
            {
                gameController.AddMissiles();
            }
            else if (name.Contains("blast"))
            {
                gameController.AddBlasts();
            }
            Destroy(gameObject);
        }
        else
        {
            return;
        }
    }
}
