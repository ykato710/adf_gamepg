﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Done_Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class Done_PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Done_Boundary boundary;
    private Done_GameController gameController;

    public GameObject shot;
    public GameObject missile;
    public GameObject blast;
    public Transform shotSpawn;
    public float fireRate;

    private float nextFire;
    public float missileNextFire;
    public float blastNextFire;
    private bool canShot;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Z) && Time.time > nextFire && canShot)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            audio.Play();
        }
        else if (Input.GetKey(KeyCode.X) && Time.time > nextFire && canShot && gameController.GetRestMissiles() != 0)
        {
            nextFire = Time.time + missileNextFire;
            Instantiate(missile, shotSpawn.position, shotSpawn.rotation);
            gameController.ReduceRestMissiles();
        }
        else if (Input.GetKey(KeyCode.C) && Time.time > nextFire && canShot && gameController.GetRestBlasts() != 0)
        {
            nextFire = Time.time + blastNextFire;
            Instantiate(blast, shotSpawn.position, shotSpawn.rotation);
            gameController.ReduceRestBlasts();
        }

        if (Input.GetKeyDown(KeyCode.B))
            gameController.Bomb();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rigidbody.velocity = movement * speed;

        rigidbody.position = new Vector3
        (
            Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
            -0.5f,
            Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
        );

    }

    public void SetShotEnable(bool value)
    {
        canShot = value;
    }

    public GameObject SetEnableTest(string str, bool value)
    {
        transform.Find(str).gameObject.SetActive(value);
        return gameObject;
    }
}
