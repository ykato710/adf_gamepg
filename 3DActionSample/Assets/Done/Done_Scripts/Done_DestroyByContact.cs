﻿using UnityEngine;
using System.Collections;

public class Done_DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject blastExplosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private Done_GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary" || other.tag == "Enemy" || other.tag == "item" || other.tag == "Invincible")
        {
            return;
        }

        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
        }

        if (other.tag == "Blast")
        {
            Instantiate(blastExplosion, other.transform.position, other.transform.rotation);
        }

        string newTag = other.tag;
        Vector3 newPosition = transform.position;
        Quaternion newQuaternion = transform.rotation;
        if (other.tag != "Shield" && other.tag != "Explosion")
            Destroy(other.gameObject);
        Destroy(gameObject);
        if (explosion != null)
            gameController.SpawnItem(newPosition, newQuaternion);
        
        if (other.tag == "Player")
        {
            other.tag = "Untagged";
            gameController.PlayerKilled();
        }
        gameController.AddScore(scoreValue);
    }

}