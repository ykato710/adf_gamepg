﻿using UnityEngine;
using System.Collections;

public class AutoDestroyer : MonoBehaviour
{

    private float startTime;
    public float timeLimit;

    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > startTime + timeLimit)
        {
            ParticleSystem ps = transform.GetComponent<ParticleSystem>();
            ps.loop = false;
        }
        if (Time.time > startTime + timeLimit*1.5)
        {
            Destroy(gameObject);
        }
    }
}
