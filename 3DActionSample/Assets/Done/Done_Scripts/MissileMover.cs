﻿using UnityEngine;
using System.Collections;

public class MissileMover : MonoBehaviour
{

    public float speed;
    public float rotSpeed;
    private GameObject target;

    void Start()
    {
        target = GetNearestEnemy();
    }

    void Update()
    {
        if (target == null || !target.activeSelf)
        {
            transform.rotation = Quaternion.identity;
            rigidbody.velocity = transform.forward * speed;
            Debug.Log("null");
        }
        else
        {
            Vector3 vecTarget = target.transform.position - transform.position; // ターゲットへのベクトル
            Vector3 vecForward = transform.TransformDirection(Vector3.forward);   // 弾の正面ベクトル
            float angleDiff = Vector3.Angle(vecForward, vecTarget);            // ターゲットまでの角度
            float angleAdd = (rotSpeed * Time.deltaTime);                    // 回転角
            Quaternion rotTarget = Quaternion.LookRotation(vecTarget);              // ターゲットへ向けるクォータニオン
            if (angleDiff <= angleAdd)
            {
                // ターゲットが回転角以内なら完全にターゲットの方を向く
                transform.rotation = rotTarget;
            }
            else
            {
                // ターゲットが回転角の外なら、指定角度だけターゲットに向ける
                float t = (angleAdd / angleDiff);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotTarget, t);
            }

            // 前進
            transform.position += transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime;
        }
    }

    private GameObject GetNearestEnemy()
    {
        float tmpDis = 0;           //距離用一時変数
        float nearDis = 0;          //最も近いオブジェクトの距離
        GameObject targetObj = null; //オブジェクト

        //タグ指定されたオブジェクトを配列で取得する
        foreach (GameObject obs in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (obs.name.Contains("Ship"))
            {
                //自身と取得したオブジェクトの距離を取得
                tmpDis = Vector3.Distance(obs.transform.position, transform.position);

                //オブジェクトの距離が近いか、距離0であればオブジェクト名を取得
                //一時変数に距離を格納
                if (nearDis == 0 || nearDis > tmpDis)
                {
                    nearDis = tmpDis;
                    targetObj = obs;
                }
            }
        }
        //最も近かったオブジェクトを返す
        return targetObj;
    }
}
