﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject bigExplosion;
    public GameObject blastExplosion;
    public GameObject playerExplosion;
    public int bossHP;
    public int scoreValue;
    private Done_GameController gameController;
    private int currentHP;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        currentHP = bossHP;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary" || other.tag == "Enemy" || other.tag == "item" || other.tag == "Explosion" || other.tag == "Invincible")
        {
            return;
        }

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            gameController.PlayerKilled();
        }

        if (--currentHP == 0 || other.tag == "Blast")
        {
            Instantiate(bigExplosion, transform.position, transform.rotation);
            gameController.AddScore(scoreValue);
            Vector3 newPosition = transform.position;
            Quaternion newQuaternion = transform.rotation;
            Destroy(other.gameObject);
            Destroy(gameObject);
            gameController.SpawnItem(newPosition, newQuaternion);
            gameController.UpdateBossStatus(true);
        }
        else
        {
            Instantiate(explosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
        }
    }
}
