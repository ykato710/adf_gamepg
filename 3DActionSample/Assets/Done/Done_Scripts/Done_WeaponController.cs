﻿using UnityEngine;
using System.Collections;

public class Done_WeaponController : MonoBehaviour
{
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    public float delay;
    private bool _isDestroy = false;

    void OnDestroy()
    {
        _isDestroy = true;
    }

    void Start()
    {
        InvokeRepeating("Fire", delay, fireRate);
    }

    void Fire()
    {
        if (_isDestroy) return;
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        audio.Play();
    }
}
