﻿using UnityEngine;
using System.Collections;

public class Done_DestroyByBoundary : MonoBehaviour
{
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name.Contains("Asteroid"))
        {
            Destroy(other.gameObject.transform.parent.gameObject);
        }
        else if (other.gameObject.tag != "Player" || !other.gameObject.name.Contains("Boss"))
        {
            Destroy(other.gameObject);
        }
    }
}