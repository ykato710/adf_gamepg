/// <summary>
/// 
/// </summary>

using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))]

//Name of class must be name of file as well

public class LocomotionPlayer : MonoBehaviour
{
    private GameController mGameController;

    private AudioSource[] mAudioHappy;
    private AudioSource[] mAudioSad;
    private AudioSource[] mAudioJump;
    private AudioSource[] mAudioKill;

    public GameObject explosion;
    public GameObject getEffect;

    protected Animator mAnimator;
    protected AnimatorStateInfo mAnimatorStateInfo;
    public Camera mCamera;

    public float jumpSpeed = 15.0f;
    public float gravity = 20.0f;
    public float hoverSpeed = 6.0f;

    private float moveDamping = 8.0f;
    private float speed = 0.0f;
    private float direction = 0;
    private Locomotion locomotion = null;
    private CharacterController controller;
    private Vector3 prev = Vector3.zero;

    private Vector3 moveDirection = Vector3.zero;
    private Quaternion currentQuaternion;

    // Use this for initialization
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            mGameController = gameControllerObject.GetComponent<GameController>();
        }
        if (mGameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }

        mAnimator = GetComponent<Animator>();
        locomotion = new Locomotion(mAnimator);
        controller = GetComponent<CharacterController>();
        prev = transform.position;
        currentQuaternion = mCamera.transform.rotation;
        mAnimatorStateInfo = mAnimator.GetCurrentAnimatorStateInfo(0);
        gameObject.tag = "Player(Active)";

        AudioSource[] audioSources = GetComponents<AudioSource>();

        mAudioHappy = new AudioSource[2];
        Array.Copy(audioSources, 0, mAudioHappy, 0, 2);
        mAudioSad = new AudioSource[2];
        Array.Copy(audioSources, 2, mAudioSad, 0, 2);
        mAudioJump = new AudioSource[3];
        Array.Copy(audioSources, 4, mAudioJump, 0, 3);
        mAudioKill = new AudioSource[4];
        Array.Copy(audioSources, 7, mAudioKill, 0, 4);
    }

    void Update()
    {
        if (mAnimator && Camera.main)
        {
            JoystickToEvents.Do(transform, Camera.main.transform, ref speed, ref direction);
            locomotion.Do(speed * 6, direction * 180);
        }

        Vector3 diff = transform.position - prev;
        diff.y = 0;

        //地面についているかどうか
        if (CheckGrounded())
        {
            mAnimator.SetBool("OnGround", true);
            moveDirection = Vector3.zero;
            currentQuaternion = mCamera.transform.rotation;

            //ジャンプ
            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpSpeed;
                mAudioJump[UnityEngine.Random.Range(0, mAudioJump.Length)].Play();
            }
        }
        else
        {
            Vector3 internalDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            internalDirection = currentQuaternion * internalDirection;
            internalDirection *= hoverSpeed;

            moveDirection = new Vector3(internalDirection.x, moveDirection.y, internalDirection.z);
            //transform.Rotate(0, Input.GetAxis("Horizontal"), 0);
            mAnimator.SetBool("OnGround", false);
            // 重力を計算
            moveDirection.y -= gravity * Time.deltaTime;

            //空中回転
            if (diff.magnitude > 0.01)
            {
                var newRotation = Quaternion.LookRotation(diff);
                newRotation.x = 0.0f;
                newRotation.z = 0.0f;
                //transform.LookAt(transform.position + diff);
                transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * moveDamping);
            }

            //if (prev.y > transform.position.y )
            //    CheckLanding();
        }

        prev = transform.position;
        // 移動
        controller.Move(moveDirection * Time.deltaTime);
    }

    public bool CheckGrounded()
    {
        //CharacterController取得
        CharacterController controller = GetComponent<CharacterController>();
        //CharacterControlle.IsGroundedがtrueならRaycastを使わずに判定終了
        if (controller.isGrounded) { return true; }
        //放つ光線の初期位置と姿勢
        //若干身体にめり込ませた位置から発射しないと正しく判定できない時がある
        Ray ray = new Ray(this.transform.position + Vector3.up * 0.1f, Vector3.down);
        //探索距離
        float tolerance = 0.3f;
        //Raycastがhitするかどうかで判定
        //地面にのみ衝突するようにレイヤを指定する

        return Physics.Raycast(ray, tolerance, LayerMask.GetMask("Ground"));
    }

    void CheckLanding()
    {
        Ray ray = new Ray(this.transform.position + Vector3.up * 0.1f, Vector3.down);
        float tolerance = 0.5f;
        if (Physics.Raycast(ray, tolerance, LayerMask.GetMask("Ground")))
        {
            //animator.SetTrigger("OnLanding");
            Debug.Log("check");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        CharacterController enemy = other as CharacterController;
        if (enemy != null)
        {
            if (other.tag == "Enemy")
            {
                Destroy(other.gameObject);
                Instantiate(explosion, other.transform.position, other.transform.rotation);
                int score = 100;
                mGameController.AddScore(score);
                mAudioKill[UnityEngine.Random.Range(0, mAudioKill.Length)].Play();
            }
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Block" && moveDirection.y > 0)
            moveDirection.y = 0;
        if (hit.gameObject.tag == "Item")
        {
            //GCから操作
            Destroy(hit.gameObject);
            GameObject effect = (GameObject)Instantiate(getEffect, transform.position + Vector3.up, transform.rotation);
            effect.transform.parent = transform;
            int score = 0;
            if (hit.gameObject.name.Contains("Coin"))
            {
                score = 1000;
            }
            mGameController.AddScore(score);
            mAudioHappy[UnityEngine.Random.Range(0, mAudioHappy.Length)].Play();
        }
        if (hit.gameObject.tag == "Enemy")
        {
            int score = -50;
            mGameController.AddScore(score);
            Destroy(hit.gameObject);
            Instantiate(explosion, hit.transform.position, hit.transform.rotation);
            mAnimator.SetTrigger("Damaged");
            mAudioSad[UnityEngine.Random.Range(0, mAudioSad.Length)].Play();
            //敵から一定距離はなれる?
        }
    }
}
