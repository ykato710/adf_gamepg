﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{

    public GameObject[] spawnPoints;
    public float spawnWait;
    private float nextSpawn;
    private bool[] canSpawn;
    private int enemyNum;
    public int maxEnemyNum = 5;
    public GameObject[] enemies;
    public GameObject BossEnemy;
    public GameObject spark;
    private int currentScore;
    private bool isBossExist;

    private Text mText;

    // Use this for initialization
    void Start()
    {
        currentScore = 0;
        enemyNum = 0;
        nextSpawn = Time.time;
        canSpawn = new bool[spawnPoints.Length];
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            canSpawn[i] = true;
        }
        GameObject textGUI = GameObject.Find("Text");
        mText = textGUI.GetComponent<Text>();
        mText.text = "Score: " + currentScore;
        isBossExist = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn && enemyNum < maxEnemyNum && !isBossExist)
        {
            int spawnNumber = Random.Range(0, spawnPoints.Length);
            if (canSpawn[spawnNumber])
            {
                GameObject spawnPoint = spawnPoints[spawnNumber];
                GameObject enemy = enemies[Random.Range(0, enemies.Length)];
                Instantiate(spark, spawnPoint.transform.position + Vector3.up, spawnPoint.transform.rotation);
                GameObject enemyObject = Instantiate(enemy, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
                canSpawn[spawnNumber] = false;
                EnemyController enecon = enemyObject.GetComponent<EnemyController>();
                enecon.SetNum(spawnNumber);
                enemyNum++;
                nextSpawn += spawnWait;
            }
        }
    }

    public void AddScore(int score)
    {
        currentScore += score;
        if (currentScore < 0) currentScore = 0;
        if (mText != null)
            mText.text = "Score: " + currentScore;
    }

    public void UpdateArray(int num)
    {
        canSpawn[num] = true;
        enemyNum--;
    }
}
