﻿using UnityEngine;
using System.Collections;

public class NewItemScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        rigidbody.AddForce(Vector3.up * 8.0f, ForceMode.Impulse);
        Vector3 randomVector = Random.insideUnitSphere;
        randomVector.y = 0;
        randomVector.Normalize();
        rigidbody.AddForce(randomVector * 3.0f, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody.angularVelocity = Vector3.up * 3.0f;
    }
}
