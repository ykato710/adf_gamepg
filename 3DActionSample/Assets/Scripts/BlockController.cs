﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour
{
    private Vector3 start;
    public float spawnWait = 5.0f;
    public float pop = 5.0f;
    public float gravity = 20.0f;
    private CharacterController controller;
    private Vector3 moveDirection = Vector3.zero;
    public GameObject[] items;
    private float nextSpawn;

    // Use this for initialization
    void Start()
    {
        moveDirection = Vector3.zero;
        start = transform.position;
        controller = GetComponent<CharacterController>();
        nextSpawn = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        moveDirection = new Vector3(0, moveDirection.y, 0);
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player(Active)")
        {

            if (Time.time > nextSpawn)
            {
                //アイテム生成位置
                Vector3 spawnPosition = transform.position;
                spawnPosition.y = spawnPosition.y + 1.2f;
                Quaternion spawnRotation = transform.rotation;

                //アイテム生成
                GameObject item = items[Random.Range(0, items.Length)];
                GameObject newItem = Instantiate(item, spawnPosition, spawnRotation) as GameObject;
                //Instantiate(item, spawnPosition, spawnRotation);
                if (newItem.name.Contains("Coin"))
                    newItem.transform.Rotate(90.0f, 0, 0);

                //アイテム出現時間更新
                nextSpawn = Time.time + spawnWait;
            }
            //ブロックを跳ねさせる
            moveDirection.y = pop;


        }
    }
}
