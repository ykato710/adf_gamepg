﻿using UnityEngine;
using System.Collections;

public class TestCameraController : MonoBehaviour
{
    /*
    This camera smoothes out rotation around the y-axis and height.
    Horizontal Distance to the target is always fixed.

    There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

    For every of those smoothed values we calculate the wanted value and the current value.
    Then we smooth it using the Lerp function.
    Then we apply the smoothed values to the transform's position.
    */

    // The target we are following
    public Transform target;
    // The distance in the x-z plane to the target
    public float distance = 6.0f;
    // the height we want the camera to be above the target
    public float height = 4.0f;
    // How much we 
    public float heightDamping = 2.0f;
    public float rotationDamping = 2.0f;
    public float moveDamping = 2.0f;
    public float lookAtAngle = 0.0f;
    private float targetAngle;
    private bool cameraMode;

    void Start()
    {
        cameraMode = true;
        Vector3 pos = target.position - Vector3.forward * distance;
        pos.y = target.transform.position.y + height;
        transform.position = pos;
        transform.LookAt(target);

    }

    void Update()
    {
        if (cameraMode)
        {
            lookAtAngle += Input.GetAxis("Mouse ScrollWheel") * 30;
            if (lookAtAngle >= 360) lookAtAngle -= 360;
            if (lookAtAngle <= -360) lookAtAngle += 360;
        }
        else
        {
            lookAtAngle = 0.0f;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            cameraMode = !cameraMode;
        }
    }

    public void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        //追従先位置
        float wantedRotationAngle = target.eulerAngles.y + lookAtAngle;
        if (cameraMode)
            wantedRotationAngle = transform.eulerAngles.y + lookAtAngle;
        float wantedHeight = target.position.y + height;

        //現在位置
        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;
        //追従先へのスムーズ移動距離(方向)
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle,
            rotationDamping * Time.deltaTime);
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        //カメラの移動
        Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
        Vector3 pos = target.position - currentRotation * Vector3.forward * distance;
        pos.y = currentHeight;
        //transform.position = pos;
        transform.position = Vector3.Lerp(transform.position, pos, moveDamping * Time.deltaTime);

        Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 0.5f * Time.deltaTime);
    }
}