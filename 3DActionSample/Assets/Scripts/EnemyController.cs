﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    private GameController gameController;
    private GameObject target;
    public float speed;
    public float gravity = 20.0f;
    public int score;
    private int enemyNum;
    private float startTime;
    public float startWait;
    private bool isActive;

    //private Vector3 spawnedPosition;

    // Use this for initialization
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        Renderer slimeRenderer = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        slimeRenderer.enabled=false;
        Collider collider = gameObject.GetComponentInChildren<SphereCollider>();
        collider.enabled = false;
        gameObject.tag = "Untagged";
        animation.Play("Wait");
        //spawnedPosition = transform.position;
        target = null;
        //target = GameObject.FindGameObjectWithTag("Player(Active)");
        startTime = Time.time;
        isActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > startTime + startWait && !isActive)
        {
            isActive = true;
            Renderer slimeRenderer = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
            slimeRenderer.enabled = true;
            Collider collider = gameObject.GetComponentInChildren<SphereCollider>();
            collider.enabled = true;
            gameObject.tag = "Enemy";
        }

        if (target != null && isActive)
        {
            animation.Play("Walk");
            CharacterController controller = GetComponent<CharacterController>();
            Vector3 moveDirection = Vector3.zero;
            //Debug.Log(target.name+" "+target.transform.position+" "+target.transform.rotation);
            
            Vector3 targetDirection = target.transform.position;
            //敵が浮かない様に縦座標は0に固定します
            targetDirection.y = transform.position.y;
            //敵の向きを自分に向かせます
            Quaternion rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirection - transform.position), 0.5f);
            //transform.LookAt(target.transform);
            rotation.x = 0;
            rotation.z = 0;
            transform.rotation = rotation;
            //敵のスピードと重力か次の位置を計算して移動させます
            moveDirection += transform.forward * 1;
            moveDirection.y -= gravity * Time.deltaTime;
            controller.Move(moveDirection * Time.deltaTime * speed);
            //controller.Move(transform.TransformDirection(Vector3.forward) * Time.deltaTime * speed);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player(Active)")
            target = other.gameObject;

    }

    void OnTriggerStay()
    {

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player(Active)")
        {
            target = null;
            animation.Play("Wait");
        }
    }

    public void SetNum(int num)
    {
        enemyNum = num;
    }

    void OnDestroy()
    {
        gameController.UpdateArray(enemyNum);
    }
}
