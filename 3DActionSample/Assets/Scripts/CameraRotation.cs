﻿using UnityEngine;
using System.Collections;
using System;

public class CameraRotation : MonoBehaviour
{
    public Transform target;	// オブジェクト
    public float distance = 4.0f;	// オブジェクトからカメラまでの距離(円運動の半径)
    public float angle = 0.0f;	// ラジアン値
    public float height = 2.5f;
    public float heightDamping = 2.0f;
    public float rotationDamping = 0.0f;
    public float lookAtAngle = 0.0f;

    public void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        //追従先位置
        float wantedRotationAngle = target.eulerAngles.y;
        float wantedHeight = target.position.y + height;

        //現在位置
        float currentRotationAngle = transform.eulerAngles.y;
        float currentHeight = transform.position.y;

        //追従先へのスムーズ移動距離(方向)
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle,
            rotationDamping * Time.deltaTime);
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

        //カメラの移動
        var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
        Vector3 pos = target.position - currentRotation * Vector3.forward * distance;
        pos.y = currentHeight;
        transform.position = pos;

        transform.LookAt(target);
    }
}
