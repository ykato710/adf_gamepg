﻿using UnityEngine;
using System.Collections;

public class CatchUp : MonoBehaviour
{

    public Transform target;
    public Camera mainCamera;

    private bool cameraMode;
    public float rotationDamping;
    public float moveDamping = 2.0f;

    // Use this for initialization
    void Start()
    {
        transform.position = target.position;
        cameraMode = true;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.transform.position, moveDamping * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.X) || Input.GetButtonDown("Rotate"))
        {
            cameraMode = !cameraMode;
        }
        if (!cameraMode)
        {
            //transform.rotation = target.transform.rotation;
        }

    }

    public void LateUpdate()
    {
        if (!cameraMode)
        {
            if (target == null)
            {
                return;
            }

            //追従先位置
            float wantedRotationAngle = target.eulerAngles.y;

            //現在位置
            float currentRotationAngle = transform.eulerAngles.y;
            //追従先へのスムーズ移動距離(方向)
            currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle,
                rotationDamping * Time.deltaTime);

            //カメラの移動
            Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
            transform.rotation = currentRotation;
        }
    }
}
